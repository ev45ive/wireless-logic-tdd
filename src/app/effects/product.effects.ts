import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as productsActions from "../actions/products.actions";
import { HttpClient } from "@angular/common/http";
import {
  map,
  mergeAll,
  switchAll,
  switchMap,
  catchError
} from "rxjs/operators";
import { ProductsSearchService } from "../shop/services/products-search.service";
import { of, Subject, Observable, from } from "rxjs";
import { Action } from "rxjs/internal/scheduler/Action";
import { State } from "../reducers";
import { Store } from "@ngrx/store";

@Injectable()
export class ProductEffects {
  constructor(
    private actions$: Actions,
    private store: Store<State>,
    private products: ProductsSearchService,
    private http: HttpClient
  ) {
    // actions$.subscribe( obs => {
    //   obs.subscribe(...)
    // })
  }

  searchProducts$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          productsActions.searchProducts /* ,productsActions.otherAction */
        ),
        switchMap(
          action =>
            this.products.fetchProducts({ query: action.query }).pipe(
              map(
                products => productsActions.loadProductsSuccess({ products }),
                catchError(err => {
                  return of(
                    productsActions.loadProductsFailure({ error: err })
                  );
                })
              )
            )
          // mergeMap(a => obs)
          // concatMap(a => obs)
          // exhaustMap(a => obs),
          // switchMap(a => obs)
          // mergeAll() concatAll() exhaust(), switchAll()
        )
      ),
    {
      // dispatch:false,
      resubscribeOnError: true
    }
  );

  searchProductsMulti$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          productsActions.searchProducts /* ,productsActions.otherAction */
        ),
        switchMap(action => {
          // const actions = new Subject<Action>();
          // return actions;
          // this.store.dispatch()
          return from([
            /* action1, action2 */
          ]);
        })
      ),
    {
      dispatch: false
    }
  );
}
