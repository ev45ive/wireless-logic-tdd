import { Spectator, createComponentFactory, SpectatorHost, createHostFactory } from '@ngneat/spectator';

import { ProductsListComponent } from './products-list.component';
import { createComponent } from '@angular/compiler/src/core';

fdescribe('ProductsListComponent', () => {
  let spectator: SpectatorHost<ProductsListComponent>;
  const createHost = createHostFactory(ProductsListComponent);

  it('should render products', () => {
    spectator = createHost(`<app-products-list [products]="prodData"></app-products-list>`, {
      hostProps: {
        prodData: [
          {name:'Prod 1'},
          {name:'Prod 2'}
        ]
      }
    });
    // spectator.detectChanges()
    expect(spectator.queryAll('.product')).toHaveLength(2)
    expect(spectator.component).toBeTruthy();
  });
});
