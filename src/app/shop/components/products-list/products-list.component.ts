import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from "@angular/core";
import { Product } from "../../products-search/Product";

@Component({
  selector: "app-products-list",
  templateUrl: "./products-list.component.html",
  styleUrls: ["./products-list.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsListComponent implements OnInit {
  @Input()
  products: Product[];
  
  @Input()
  selected: Product;

  @Output()
  selectedChange = new EventEmitter<Product>();

  select(product: Product) {
    this.selectedChange.emit(product);
    // this.selected = product
  }

  random(){
    console.log('I am rechecked!',Math.random())
  }

  constructor() {}

  ngOnInit() {}
}
