import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  SimpleChanges
} from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.css"]
})
export class SearchFormComponent implements OnInit {
  searchForm = new FormGroup({
    query: new FormControl("", [])
  });

  // @Input() query: string;

  // ngDoCheck(){}
  // ngOnChanges(changes:SimpleChanges){
  //   this.searchForm.get('query').setValue(changes['query'].currentValue)
  // }
  @Input() set query(q) {
    this.searchForm.get("query").setValue(q);
  }

  @Output()
  searchChange = new EventEmitter<string>();

  search(query: string) {
    this.searchChange.emit(query);
  }

  constructor() {}

  ngOnInit() {}
}
