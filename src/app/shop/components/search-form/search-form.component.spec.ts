import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { SearchFormComponent } from './search-form.component';

describe('SearchFormComponent', () => {
  let spectator: Spectator<SearchFormComponent>;
  const createComponent = createComponentFactory(SearchFormComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
