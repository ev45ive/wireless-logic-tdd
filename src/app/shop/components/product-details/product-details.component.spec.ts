import { Spectator, createComponentFactory } from '@ngneat/spectator';

import { ProductDetailsComponent } from './product-details.component';

describe('ProductDetailsComponent', () => {
  let spectator: Spectator<ProductDetailsComponent>;
  const createComponent = createComponentFactory(ProductDetailsComponent);

  it('should create', () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });
});
