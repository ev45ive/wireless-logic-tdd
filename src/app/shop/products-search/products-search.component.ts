import { Component, OnInit } from "@angular/core";
import { ProductsSearchService } from "../services/products-search.service";
import { Product } from "./Product";
import { Subscription, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-products-search",
  templateUrl: "./products-search.component.html",
  styleUrls: ["./products-search.component.css"]
})
export class ProductsSearchComponent implements OnInit {
  queries$ = this.service.queries;
  isLoading$ = this.service.isLoading;
  results$ = this.service.results$;

  selected: Product;

  constructor(private service: ProductsSearchService) {}

  search(query: string) {
    this.service.searchProducts({ query });
  }

  select(product: Product) {
    this.selected = product;
  }

  ngOnInit() {}
}
