import { Spectator, createComponentFactory } from "@ngneat/spectator";
import { MockComponent } from "ng-mocks";
import { ProductsSearchComponent } from "./products-search.component";
import { NO_ERRORS_SCHEMA } from "@angular/compiler/src/core";
import { CUSTOM_ELEMENTS_SCHEMA, Component } from "@angular/core";
import { SearchFormComponent } from "../components/search-form/search-form.component";
import { ProductsListComponent } from "../components/products-list/products-list.component";
import { ProductDetailsComponent } from "../components/product-details/product-details.component";

// https://www.npmjs.com/package/ng-mocks

// @Component({
//   selector:'app-search-form',
//   template:'<ng-content></ng-content>'
// })
// export class SearchFormMock{}

fdescribe("ProductsSearchComponent", () => {
  let spectator: Spectator<ProductsSearchComponent>;
  const createComponent = createComponentFactory({
    component: ProductsSearchComponent,
    declarations: [
      MockComponent(SearchFormComponent),
      MockComponent(ProductsListComponent),
      MockComponent(ProductDetailsComponent)
    ]
    // schemas: [/* NO_ERRORS_SCHEMA */ CUSTOM_ELEMENTS_SCHEMA]
  });

  it("should create", () => {
    spectator = createComponent();

    expect(spectator.component).toBeTruthy();
  });

  it("should have products search view", () => {
    spectator = createComponent();
    expect(spectator.query(SearchFormComponent)).toExist();
    // expect("app-search-form").toExist();
  });

  it("should have products search results view", () => {
    spectator = createComponent();
    expect(spectator.query(ProductsListComponent)).toExist();
    spectator.component.products = [1,2,3]
    spectator.detectChanges()
    expect(spectator.query(ProductsListComponent).products).toEqual(spectator.component.products)
  });

  it("should have product details view", () => {
    spectator = createComponent();
    expect(spectator.query(ProductDetailsComponent)).toExist();
  });
});
