import { TestBed } from '@angular/core/testing';

import { ProductsSearchService } from './products-search.service';

describe('ProductsSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductsSearchService = TestBed.get(ProductsSearchService);
    expect(service).toBeTruthy();
  });
});
