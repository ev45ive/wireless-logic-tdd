import { Injectable } from "@angular/core";

import { Store } from "@ngrx/store";
import { ProductsState } from "src/app/reducers/products.reducer";
import { selectProductsSearchResults } from "src/app/selectors/products.selectors";
import { HttpClient } from "@angular/common/http";
import * as productsActions from "src/app/actions/products.actions";
import { Product } from "../products-search/Product";

@Injectable({
  providedIn: "root"
})
export class ProductsSearchService {
  isLoading = this.store.select(state => state.products.isLoading);
  queries = this.store.select(state => state.products.query);
  results$ = this.store.select(selectProductsSearchResults);

  searchProducts(params: { query: string }) {
    this.store.dispatch(
      productsActions.searchProducts({ query: params.query })
    );

    // this.store.dispatch(productsActions.loadProducts());
    // this.fetchProducts(params).subscribe({
    //   next: products =>
    //     this.store.dispatch(productsActions.loadProductsSuccess({ products })),
    //   error: error =>
    //     this.store.dispatch(productsActions.loadProductsFailure({ error }))
    // });
  }

  constructor(
    private http: HttpClient,
    private store: Store<{ products: ProductsState }>
  ) {}

  fetchProducts(params: { query: string }) {
    return this.http.get<Product[]>("http://localhost:3000/books/", {
      params: { q: params.query }
    });
  }
}
