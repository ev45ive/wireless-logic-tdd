import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShopRoutingModule } from './shop-routing.module';
import { ProductsSearchComponent } from './products-search/products-search.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SyncSearchComponent } from './views/sync-search/sync-search.component';
import { StoreModule } from '@ngrx/store';
import * as fromProducts from '../reducers/products.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ProductEffects } from '../effects/product.effects';


@NgModule({
  declarations: [
    ProductsSearchComponent, 
    SearchFormComponent, 
    ProductsListComponent, 
    ProductDetailsComponent, SyncSearchComponent
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    ShopRoutingModule,
    StoreModule.forFeature(fromProducts.productsFeatureKey, fromProducts.reducer),
    EffectsModule.forFeature([ProductEffects])
  ]
})
export class ShopModule { }
