import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsSearchComponent } from './products-search/products-search.component';
import { SyncSearchComponent } from './views/sync-search/sync-search.component';


const routes: Routes = [
  {
    path:'',
    component: SyncSearchComponent
  },
  {
    path:'',
    component: ProductsSearchComponent
  },
  {
    path:'details/:product_id',
    component: ProductsSearchComponent
  },
  {
    path:'shopping_cart',
    component: ProductsSearchComponent
  },
  {
    path:'checkout',
    component: ProductsSearchComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule { }
