import { ProductsState } from "../reducers/products.reducer";
import { createSelector } from "@ngrx/store";

type State = {
  products: ProductsState;
};

export const selectQuery = (state: State) => state.products.query;

const selectProducts = (state: State) => state.products;
const selectEntities = state => state.entities;
const selectResults = state => state.results;

export const selectSearchResults = createSelector(
  selectEntities,
  selectResults,
  (entities, list) => list.map(id => entities[id])
);

export const selectProductsSearchResults = createSelector(
  selectProducts,
  selectSearchResults
);
