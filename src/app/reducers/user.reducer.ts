import { Action, createReducer, on } from "@ngrx/store";
import * as UserActions from "./user.actions";
export const userFeatureKey = "user";

export interface State {
  isLoggedIn: boolean;
  currentUser: {
    name: string;
  } | null;
}

export const initialState: State = {
  isLoggedIn: false,
  currentUser: null
};

const userReducer = createReducer(
  initialState,
  on(UserActions.LoggedIn, (state, action) => ({ ...state, isLoggedIn: true })),
  on(UserActions.LoggedOut, (state, action) => ({
    ...state,
    isLoggedIn: false,
    currentUser: null
  })),
  on(UserActions.UpdateCurrentUser, (state, action) => ({
    ...state,
    currentUser: action.user
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return userReducer(state, action);
}
