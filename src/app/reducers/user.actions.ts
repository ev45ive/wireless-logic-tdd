import { createAction } from "@ngrx/store";
export const LoggedIn = createAction("[User] LoggedIn");
export const LoggedOut = createAction("[User] LoggedOut");

type User = {
  name: string;
};

export const UpdateCurrentUser = createAction(
  "[User] Update Current User",
  (user: User) => ({ user })
);
