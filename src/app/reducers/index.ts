import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from "@ngrx/store";
import { environment } from "../../environments/environment";
import * as fromUsers from "../reducers/user.reducer";
// import * as fromProducts from "../reducers/products.reducer";

export interface State {}

export const reducers: ActionReducerMap<State> = {
  [fromUsers.userFeatureKey]: fromUsers.reducer,
  
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];
