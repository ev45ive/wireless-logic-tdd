import { Action, createReducer, on } from "@ngrx/store";
import { Product } from "../shop/products-search/Product";
import * as ProductActions from "../actions/products.actions";
export const productsFeatureKey = "products";

export interface ProductsState {
  query: string;
  isLoading: boolean;
  // products: Product[];
  // products:Record<number,Product>
  entities: {
    [id: number]: Product;
  };
  results: Product["id"][];
  homePageList: Product["id"][];
  error: Error | null;
}

export const initialState: ProductsState = {
  isLoading: false,
  query: "",
  error: null,
  entities: {},
  results: [],
  homePageList: []
};

const productsReducer = createReducer(
  initialState,
  on(ProductActions.loadProducts, state => ({
    ...state,
    results: [],
    isLoading: true,
    error: null
  })),
  on(ProductActions.loadProductsSuccess, (state, action) => ({
    ...state,
    // https://github.com/paularmstrong/normalizr
    entities: action.products.reduce(
      (entities, p) => {
        entities[p.id] = p;
        return entities;
      },
      { ...state.entities }
    ),
    results: action.products.map(p => p.id),
    isLoading: false
  })),
  on(ProductActions.loadProductsFailure, (state, { error }) => ({
    ...state,
    isLoading: false,
    error
  }))
);

export function reducer(state: ProductsState | undefined, action: Action) {
  return productsReducer(state, action);
}
