import {
  Component,
  ChangeDetectorRef,
  ChangeDetectionStrategy
} from "@angular/core";
import { Store, select } from "@ngrx/store";
import { pluck } from "rxjs/operators";
import {
  LoggedIn,
  LoggedOut,
  UpdateCurrentUser
} from "./reducers/user.actions";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  title = "wirelesslogic-angular-tdd";

  isLoggedIn = this.store.pipe(select(state => state.user.isLoggedIn));
  currentUser = this.store.pipe(select(state => state.user.currentUser));

  constructor(
    private store: Store<{
      user: { isLoggedIn: boolean; currentUser: { name: string } | null };
    }>
  ) {}

  login() {
    // Login ..., then.. tell store I am logged in:
    this.store.dispatch(LoggedIn());
    this.store.dispatch(UpdateCurrentUser({ name: "Some user" }));
  }

  logout() {
    this.store.dispatch(LoggedOut());
  }
}
