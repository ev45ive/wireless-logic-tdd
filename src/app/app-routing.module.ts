import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomePageComponent } from "./views/home-page/home-page.component";
import { PageNotFoundComponent } from "./views/page-not-found/page-not-found.component";

const routes: Routes = [
  {
    path: "",
    component: HomePageComponent
  },
  {
    path: "shop", 
    loadChildren: () => import("./shop/shop.module").then(m => m.ShopModule)
  },
  {
    path: "**",
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
