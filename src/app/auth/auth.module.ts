import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AuthRoutingModule } from "./auth-routing.module";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [LoginComponent, RegisterComponent],
  imports: [
    CommonModule, 
    // HttpClientModule,
    AuthRoutingModule
  ]
})
export class AuthModule {
  static forRoot(config: {
    /**
     * Enpoint for oAuth ...
     */
    authUrl: string;
  }): ModuleWithProviders {
    if (!config.authUrl) {
      throw Error("Missing  config.authUrl");
    }
    return {
      ngModule: AuthModule,
      providers: [
        {
          provide: "AUTH_URL",
          useValue: config.authUrl
        }
      ]
    };
  }
}
