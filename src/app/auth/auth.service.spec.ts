import { TestBed } from "@angular/core/testing";

import { AuthService } from "./auth.service";
import { of, Observable, from } from "rxjs";
import { sequenceEqual } from "rxjs/operators";
import { HttpClient, HttpClientModule } from "@angular/common/http";

import { cold, getTestScheduler, hot } from "jasmine-marbles";
import { TestScheduler } from "rxjs/testing";

import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

describe("AuthService", () => {
  let service: AuthService;
  let httpCtrl: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: "AUTH_URL",
          useValue: "MockAuthUrl"
        }
      ]
    });
    service = TestBed.get(AuthService);
    httpCtrl = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpCtrl.verify();
  });

  /* === */

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should log in user", () => {
    const credentials = {
      username: "Test",
      password: "Pa$$word1"
    };

    const response = service.login(credentials);

    response.subscribe(result => {
      expect(result).toEqual({ success: true });
    });
    const req = httpCtrl.expectOne("MockAuthUrl/login");
    expect(req.request.body).toEqual(credentials);

    req.flush({ success: true });
  });

  it("should register user", () => {
    const credentials = {
      username: "Test",
      password: "Pa$$word1"
    };

    const response = service.register(credentials);

    response.subscribe(result => {
      expect(result).toEqual({ success: true });
    });
    const req = httpCtrl.expectOne("MockAuthUrl/register");

    expect(req.request.body).toEqual(credentials);

    req.flush({ success: true });
  });

  it("should log out user", () => {
    const response = service.logout();

    response.subscribe(result => {
      expect(result).toEqual({ success: true });
    });
    const req = httpCtrl.expectOne("MockAuthUrl/logout");

    req.flush({ success: true });
  });

  xit("should notify is user logged in", () => {
    const state: Observable<boolean> = service.loggedInState();

    const expected = cold("f--t--f", {
      t: true,
      f: false
    });

    expect(state).toBeObservable(expected);
  });
});
