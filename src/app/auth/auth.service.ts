import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, from, Subject, merge, of } from "rxjs";

interface UserRequest {
  username: string;
  password: string;
}
interface UserResponse {
  success: boolean;
}

@Injectable({
  providedIn: "root"
})
export class AuthService {

  private isLoggedIn = false;
  private isLoggedInChange = new Subject<boolean>();

  loggedInState(): Observable<boolean> {
    // return from([false, true, false]);
    
    return merge(
      of(this.isLoggedIn),
      this.isLoggedInChange
    )
  }

  constructor(
    @Inject("AUTH_URL") private auth_url: string,
    private http: HttpClient
  ) {}

  login(request: UserRequest) {
    return this.http.post<UserResponse>(this.auth_url + "/login", request);
  }

  logout() {
    return this.http.post<UserResponse>(this.auth_url + "/logout", null);
  }

  register(request: UserRequest) {
    return this.http.post<UserResponse>(this.auth_url + "/register", request);
  }
}
