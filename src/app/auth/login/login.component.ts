import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  message: string = "";

  username: string = "";
  password: string = "";

  constructor(private service: AuthService) {}

  login() {
    this.service
      .login({
        username: this.username,
        password: this.password
      })
      .subscribe({
        next: () => (this.message = "Success"),
        error: () => (this.message = "Failed!")
      });
  }

  ngOnInit() {}
}
