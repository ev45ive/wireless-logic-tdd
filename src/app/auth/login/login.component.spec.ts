import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { LoginComponent } from "./login.component";
import { By } from "@angular/platform-browser";
import { AuthService } from "../auth.service";

describe("LoginComponent", () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      providers: [
        {
          provide: AuthService,
          useValue: jasmine.createSpyObj<AuthService>("MockAuthService", [
            "login"
          ])
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should display messages", () => {
    component.message = "Pancakes";
    fixture.detectChanges();

    var elem = fixture.debugElement.query(
      //
      By.css(".message")
    );

    expect(elem.nativeElement.innerText).toMatch("Pancakes");
  });

  it("should show empty login form", () => {
    const username = fixture.debugElement.query(By.css("input.username"));
    const password = fixture.debugElement.query(By.css("input.password"));

    expect(username).not.toBeNull("Username Input not found");
    expect(password).not.toBeNull("Password Input not found");
  });

  it("should login when form is submitted", () => {
    const service: AuthService = TestBed.get(AuthService);

    // debugger
    const form = fixture.debugElement.query(By.css("form.login-form"));
    
    const username = fixture.debugElement.query(By.css("input.username"));
    username.nativeElement.value = "TestUser";
    // username.triggerEventHandler("input", { target: username.nativeElement });
    username.nativeElement.dispatchEvent(new CustomEvent('input'))
    
    const password = fixture.debugElement.query(By.css("input.password"));
    password.nativeElement.value = "TestPassword";
    password.triggerEventHandler("input", { target: password.nativeElement });
    
    // button.nativeElement.click()
    const button = fixture.debugElement.query(By.css("input.login-button"));
    button.nativeElement.dispatchEvent(new MouseEvent("click", {}));
    // button.triggerEventHandler('click',{})

    expect(service.login).toHaveBeenCalledWith({
      username: "TestUser",
      password: "TestPassword"
    });
  });
});
