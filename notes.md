git clone https://bitbucket.org/ev45ive/wireless-logic-tdd.git
          > /src/app/auth/auth.service.spec.ts
          > /src/app/auth/auth.service.ts

ng new wirelesslogic-angular-tdd --routing
ng serve --port 4200

ng test

ng g m auth -m app --routing  
ng g s auth/auth

# Testing frameworks
https://jasmine.github.io/
https://mochajs.org/
https://jestjs.io/

# Extra matchers
https://www.chaijs.com/

# Http testing
https://angular.io/guide/http#testing-http-requests

# RxJS
https://rxjs-dev.firebaseapp.com/
https://rxmarbles.com/
https://rxviz.com/examples/today-is

# Login / Register 
ng g c auth/login
ng g c auth/register


# NgNeat / Spectator
ng i --save @ngneat/spectator

ng g @ngneat/spectator:spectator-component name
ng config cli.defaultCollection 



ng g @ngrx/schematics:reducer User --group --creators   

# Cypress
https://glebbahmutov.com/blog/use-typescript-with-cypress/


# Angular debug elem
ng.probe($0).injector.get(ng.probe($0).providerTokens[0])

# Mock time
https://jasmine.github.io/api/2.7/Clock.html

https://angular.io/api/core/testing/fakeAsync

https://docs.cypress.io/guides/guides/stubs-spies-and-clocks.html#Capabilities

